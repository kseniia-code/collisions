#ifndef SPHERE_H
#define SPHERE_H

#include <parametrics/gmpsphere>
#include "plane.h"
#include "beziersurf.h"


class Sphere : public GMlib::PSphere<float> {

public:
  using PSphere::PSphere;
    Sphere(GMlib::Vector<float,3> v, float m, float r, Beziersurf* s):GMlib::PSphere<float>(r){
        this->v = v;
        this->surf = s;
        this->m = m;
        this->x = 0;
        this->energy = m*((v*v)*0.5 + 9.81*this->getPos()(2));
    }

    GMlib::Vector<float,3> get_ds(){
        return ds;
    }

    void setUV(Beziersurf* s){
        s->estimateClpPar(this->getPos(), _u, _v);
    }

    GMlib::Vector<float,3> get_p(){
        return p;
    }

    GMlib::Vector<float,3> get_v(){
        return v;
    }

    float get_m(){
        return m;
    }

    double get_X(){
        return this->x;
    }

    double set_X(double x){
       this->x = x;
    }

    double get_energy(){
        return this->m*(0.5*(this->v*this->v) + 9.81*this->getPos()(2));
    }


        //when we collide with another ball we change the energy
    void set_v(GMlib::Vector<float,3> new_v){
        v = new_v;
        //std::cout<<" velocity new "<< v <<std::endl;
        //this->energy = this->m*(0.5*(this->v*this->v) + 9.81*this->getPos()(2));
  }

        //when we collide with wall we keep the energy
    void set_v_keep_energy(GMlib::Vector<float,3> new_v){
        v = new_v;
//        double k;
//        this->energy = this->m*(0.5*(this->v*this->v) + 9.81*this->getPos()(2));
//        k = std::sqrt(2*(this->energy/m + 9.81*this->getPos()(2)));
//        this->v = k/new_v.getLength();

    }

    ~Sphere() {}

    void updateStep(double dt);
    void moveBall (double time);

protected:
  void localSimulate(double dt);

private:
  bool m_test02 {false};

  //float r; //radius
  float m; //mass

  Beziersurf* surf;

  GMlib::Point<float,3> p;
  GMlib::Point<float,3> q;

  GMlib::Vector<float,3> v;
  //coordinates
  float _u;
  float _v;

  //normal
  GMlib::UnitVector<float,3> n;

  GMlib::Vector<float,3> ds;

  GMlib::UnitVector<float,3> axis;

  double x;

  double energy;


}; // END class Sphere



#endif // SPHERE_H

