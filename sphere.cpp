#include "sphere.h"

void Sphere::moveBall(double time){
    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);// -9.81 means this vector goes down
        this->translate(time*ds);
        //v += time*g;
        GMlib::UnitVector<float,3> n = surf->getNormal();
        //v -= (v*n)*n;
        //updateStep(time);

}

void Sphere::updateStep(double dt) {

    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);

    //finding vector ds
    ds = dt*v + (0.5*dt*dt)*g;

    //get point p
    p = this->getPos() + ds;

    surf->getClosestPoint(p, _u, _v);

    GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(_u, _v, 1, 1);

    //getting a normal to surface

    n = m[0][1]^m[1][0];
    // normalize the vector
    n.normalize();

    //finding the new position of the ball
    GMlib::Vector<float,3> newpos = m[0][0] + this->getRadius()*n;

    //update v and ds and call translate
    ds = newpos - this->getPos();

    //updating of velocity
    double vv = v*v + 2*(g*ds);
    v += dt*g;
    v -= (v*n)*n;//

    //    std::cout<<"ball nom "<< this->getName();
    //    std::cout<<" velocity old "<< vv;
    //    std::cout<<" velocity old "<< v*v<<std::endl;
    //    std::cout<<" velocity new "<<(v*v - (2*(g*ds)))<<std::endl;

    if((v*v - (2*(g*ds)))<0)
        std::cout<<" message "<<std::endl;

    double nvv = v*v;

    if(nvv > 0.0001)
    {
      if(vv > 0.0001)
         v *= std::sqrt(vv/nvv);
    }

    axis = n^ds; // axis of rotation

}

void Sphere::localSimulate(double dt) {

    //moving of the ball
    this->translateParent(ds);
    rotateParent(GMlib::Angle((ds.getLength())/this->getRadius()), axis );

    //check the energy
//    std::cout<<"ball nom "<< this->getName();
//    std::cout<<" energy "<< this->get_energy()<<std::endl;

}
