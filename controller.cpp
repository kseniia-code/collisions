#include "controller.h"

//using namespace GMlib;

Controller::Controller()
{
    this->toggleVisible();

}

Controller::Controller(Beziersurf *w){

    //insert to controller
    this->insert(w);
    //our surface
    this->surf = w;

}


void Controller::insertBall(Sphere *b){

    //insert balls to the controller
    this->insert(b);
    //insert to array
    this->BallsArray += b;

}

void Controller::insertWall(Plane *w){

    //insert walls to the controller
    this->insert(w);
    //insert to array
    this->WallsArray += w;

}

void Controller::localSimulate(double dt) {

    for (int i = 0; i < BallsArray.size(); i++ )
        BallsArray[i]->updateStep(dt);

    //double energy = 0;
//    for(int i=0; i<BallsArray.size(); i++){
//        //energy += BallsArray[i]->get_energy();
//    std::cout<<"ball nom "<< BallsArray[i]->getName();
//    std::cout << "energy: " << BallsArray[i]->get_energy() <<std::endl;
//    }

    for (int i = 0; i < BallsArray.size(); i++ )
        for (int j = i+1; j < BallsArray.size(); j++)
            findBBcoll(BallsArray[i], BallsArray[j], Collision, 0);

    for (int i = 0; i < BallsArray.size(); i++ )
        for (int j = 0; j < WallsArray.size(); j++)
            findBWcoll(BallsArray[i], WallsArray[j], Collision, 0);

    while(Collision.getSize()>0){

        //sorting of sequence of collisions
        Collision.sort();

        //making it unique
        Collision.makeUnique();

        CollObj co = Collision[0];
        Collision.removeFront();

        if(co.getBW()){

            co.getBall(0)->moveBall(co.getX()*dt);
            colBW(co.getBall(0), co.getPlane(), (1-co.getX())*dt);
            // update
            co.getBall(0)->set_X(co.getX());

            for (int i = 0; i < BallsArray.size(); i++ )
                if(BallsArray[i] != co.getBall(0))
                    findBBcoll(co.getBall(0), BallsArray[i], Collision, co.getX());

            for (int i = 0; i < WallsArray.size(); i++ )
                if(WallsArray[i] != co.getPlane())
                    findBWcoll(co.getBall(0), WallsArray[i], Collision, co.getX());
        }
        else {
            co.getBall(0)->moveBall(co.getX()*dt);
            co.getBall(1)->moveBall(co.getX()*dt);

            colBB(co.getBall(0), co.getBall(1), (1-co.getX())*dt);

            co.getBall(0)->set_X(co.getX());
            co.getBall(1)->set_X(co.getX());

            //update
            for (int i = 0; i < WallsArray.size(); i++ )

            {
                findBWcoll( co.getBall(0), WallsArray[i], Collision, co.getX());
                findBWcoll( co.getBall(1), WallsArray[i], Collision, co.getX());

            }

            for (int i = 0; i < BallsArray.size(); i++ )
                if(BallsArray[i] != co.getBall(0) && BallsArray[i] != co.getBall(1))
                {

                    findBBcoll(co.getBall(0), BallsArray[i], Collision, co.getX());
                    findBBcoll(co.getBall(1), BallsArray[i], Collision, co.getX());

                }
        }
    }

   //std::cout << "general energy: " << energy <<std::endl;
}

void Controller::findBBcoll(Sphere *b1, Sphere *b2, GMlib::Array<CollObj> &co, double y){

    GMlib::Vector<float,3> s = b1->get_p() - b2->get_p();
    //GMlib::Vector<float,3> s = b1->get_p() - b2->get_p() - b1->get_X()*b1->get_ds() + b2->get_X()*b2->get_ds();
    GMlib::Vector<float,3> h = b1->get_ds() - b2->get_ds();
    //GMlib::Vector<float,3> h = (1 + b1->get_X())*b1->get_ds() - (1 + b2->get_X())*b2->get_ds() ;

    double a = h*h;
    double b = s*h;
    double r = b1->getRadius() + b2->getRadius();
    double c = s*s - r*r;

    //if 2 ball are near then we translate balls a bit
    if (c < 0){
        double t = 0.51*(r - s.getLength())/s.getLength();
        b1->translate(t*s);
        b2->translate(-t*s);
        s *= 1+2*t;
        b = s*h;
        c = s*s - r*r;
        std::cout << " translate ball-ball " << t << std::endl;
    }

    double x = (-b - sqrt(b*b - a*c))/a;
            //we choose the smallest x (two solotions - collisions, choose nearest)

    if(x > y && x <= 1) {
        //make collision object and add to collision array
        Collision.insertAlways(CollObj(b1,b2,x),true);
        std::cout << "Collision ball ball: x = " << x << std::endl;
    }
}


void Controller::findBWcoll(Sphere *b, Plane *w, GMlib::Array<CollObj> &co, double y){

    //get point p
    GMlib::Point<float,3> p = b->getPos();
    //getting the closest point on the plane
    GMlib::Vector<float,3> d = w->getCornerPoint() -  p;//get d = q - p

    //check (b->get_ds()*w->getNormal() = 0
    double dsn = b->get_ds()*w->getNormal();
    double dn = d*w->getNormal();

    //if ball and wall are near then we translate ball a bit
    if((dn+b->getRadius())>0.0)
    {
        b->translate(2*(dn+b->getRadius())*w->getNormal());
        dn -= 2*(dn+b->getRadius());
        std::cout<< "translate ball-wall" << std::endl;
    }

    //if (std::abs(dsn) > 1.0e+6){
    if (dsn < -1.0e-6){

        x = (b->getRadius() + dn)/dsn;

        if(x >y && x<=1) {
            Collision.insertAlways(CollObj(b,w,x),true);
            std::cout << "collision ball - wall: ";
            std::cout << " x= " << x << std::endl;
        }
    }
}

void Controller::colBB(Sphere *b1, Sphere *b2, double dt_c){

    GMlib::Vector<float, 3> new_v1 = b1->get_v();
    GMlib::Vector<float, 3> new_v2 = b2->get_v();

    GMlib::UnitVector<float, 3> d = b2->getCenterPos() - b1->getCenterPos();

    GMlib::Vector<float, 3> v1 = (b1->get_v() * d)*d;
    GMlib::Vector<float, 3> v1n = b1->get_v() - v1;

    GMlib::Vector<float, 3> v2 = (b2->get_v() * d)*d;
    GMlib::Vector<float, 3> v2n = (b2->get_v()) - v2;

    float m1 = b1->get_m();
    float m2 = b2->get_m();

    GMlib::Vector<float, 3> _v1 = (((m1 - m2)/(m1 + m2))*v1 + ((2*m2)/(m1 + m2))*v2); //2
    GMlib::Vector<float, 3> _v2 = (((m2 - m1)/(m1 + m2))*v2 + ((2*m1)/(m1 + m2))*v1); //2

    new_v1 = _v1 + v1n;
    new_v2 = _v2 + v2n;

    //set velocity
    b1->set_v(new_v1);
    //update ds
    b1->updateStep(dt_c);

    //set velocity
    b2->set_v(new_v2);
    //update ds
    b2->updateStep(dt_c);

}

void Controller::colBW(Sphere *b, Plane *w, double dt_c){

    //update velocity vector
    GMlib::Vector<float, 3> new_v = b->get_v();

    new_v -= 2*((b->get_v()*w->getNormal())*w->getNormal()); //coefficient - 2

    //set velocity
    //b->set_v(new_v);
    b->set_v_keep_energy(new_v);
    //update ds
    b->updateStep(dt_c);

    std::cout<<"Normal"<<w->getNormal()<< std::endl;
    //    std::cout<<dt_c<< std::endl;
    //    std::cout<<b->get_v()<< std::endl;
    //    std::cout<<new_v<< std::endl;

}


