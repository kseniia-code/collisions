#ifndef PLANE_H
#define PLANE_H

#include <parametrics/gmpplane>

class Plane : public GMlib::PPlane<float> {

public:
  using PPlane::PPlane;

  ~Plane(){}

    GMlib::Point<float,3>& getCornerPoint(){
        return this->_pt;
    }


}; // END class Plane



#endif // PLANE_H

