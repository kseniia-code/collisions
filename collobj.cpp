#include "collobj.h"

CollObj::CollObj(){}

CollObj::CollObj(Sphere *b1, Sphere *b2, double x){
    b[0]=b1;
    b[1]=b2;
    this->x=x;
    bw = false;
}

CollObj::CollObj(Sphere *b1, Plane *w, double x){
    b[0]=b1;
    this->w=w;
    this->x=x;
    bw = true;
}