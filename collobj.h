#ifndef COLLOBJ_H
#define COLLOBJ_H

#include "plane.h"
#include "sphere.h"
#include "beziersurf.h"

class CollObj {

private:
        Sphere* b[2];
        Plane* w;
        double x;
        bool bw;

public:
        CollObj(Sphere* b1, Sphere* b2, double x);
        CollObj(Sphere* b, Plane* w, double x);

        //we also should have empty constructor, because we have array of collision objects
        CollObj();

        bool operator < ( const CollObj &other) const {
            return x < other.x;
        }

        bool operator == (const CollObj &other) const{
            if (b[0] == other.b[0])                     return true;
            if (!other.bw && b[0] == other.b[1])        return true;
            if (!bw && b[1] == other.b[0])              return true;
            if (!bw && !other.bw && b[1] == other.b[1]) return true;

            return false;
        }

        Sphere* getBall(int i){
            return b[i];
        }

        Plane* getPlane(){
            return w;
        }

        double getX(){
            return x;
         }

        bool getBW(){
            return bw;
        }
};


#endif // COLLOBJ_H

