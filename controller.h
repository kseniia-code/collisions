#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "plane.h"
#include "sphere.h"
#include "collobj.h"
#include "beziersurf.h"

#include "core/gmarray"

class Controller:public GMlib::PSphere <float>{
    GM_SCENEOBJECT(Controller)

    private:

    GMlib::Array <Sphere*> BallsArray;
    GMlib::Array <Plane*> WallsArray;
    GMlib::Array <CollObj> Collision;
    Beziersurf* surf;
    double x;

    double _dt;

public:
    Controller();
    Controller(Beziersurf *w);
    void insertBall(Sphere *b);
    void insertWall(Plane *w);
    bool col = false;


protected:

    void findBBcoll(Sphere *b1,Sphere *b2, GMlib::Array<CollObj> &co, double y);
    void findBWcoll(Sphere *b, Plane *w, GMlib::Array<CollObj> &co, double y);
    void localSimulate(double dt);
    void colBB(Sphere *b1,Sphere *b2, double dt_c);
    void colBW(Sphere *b, Plane *w, double dt_c);

};

#endif // CONTROLLER_H

